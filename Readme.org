* Analítica Avanzada Y Ciencia de Datos
Primero que nada, bienvenidos a este módulo del diplomado Analítica Avanzada y Ciencia de Datos.

¡Muchas gracias por estar aquí!

* Objetivo
El objetivo de este curso es darles una introducción profunda en el lenguaje de programación R, que esten familiarizados con los ambientes y se sientan cómodos usando las diferentes herramientas que nos provee.

* ¿Cómo nos comunicamos?

Escribe un correo a ~miguel.escalante@itam.mx~ y que el sujeto del correo comience con [AACD].

* Material del Curso

La manera en la que podrán acceder al material del curso es através de la plataforma [[https://gitlab.com][gitlab]]. Generen su cuenta y en cuanto la tengan, cerraré el acceso público al repositorio.

* Evaluación

La calificación del módulo está compuesta de la manera siguiente:

| Evaluacion          | Porcentaje |
|---------------------+------------|
| Ejercicios de Clase |        60% |
| Proyecto Final      |        40% |

Recuerda que esta clase es de *construir* habilidades. La única manera de construirlas es practicando.

** Calificación de código

Se usará la siguiente escala para las tareas

|Puntos | Descripción|
|----------+----------|
|5| Excelente. Código limpio, conciso, documentado y exploró los conceptos en profundidad.|
|4| Completo y correcto. Incluye el análisis, el programa, el caso de pruebas y responde a las preguntas planteadas.|
|3| Contiene unos cuentos errores menores.|
|2| Entrega parcial o tiene errores mayores.|
|1| Le faltó mucho.|
|0|Ni lo intentó :(|

Las tareas serán evaluadas automáticamente, por lo que se recomienda leer a profundidad las instrucciones y hacer todas las preguntas que consideren necesarias.

** Aclaraciones
- El código **NO** es la respuesta.
- La gráfica **NO** es la respuesta.
-  Debes de **mostrar que entendiste la solución y el problema**.

** Código de conducta académica

*Adaptado del departamento de ciencias de la computación de Grand Valley State University y de  George Washington University)*

- Se espera que tú...
  - Crees / desarrolles tus tareas (incluyendo el código fuente).
  - Entiendas tus soluciones
  - Reconozcas la ayuda de otros en la escritura.
  - Cites la fuente en la tarea.
  - Te protejas de sospecha al no permitir que otros vean tu tarea antes de que sea enviada.
  - Contactes al profesor para aclarar los requerimientos de las tareas.
  - Uses extensivamente Github para *socializar* el conocimiento, soluciones, dudas, etc.

- Se prefiere que tú...
  - Discutas diversos caminos para alcanzar la solución.
  - Compartas tu conociemiento con otros estudiantes acerca de errores de sintáxis, trucos de código, etc.
  - Proveas y recibas ayuda respecto a errores de ejecución.
  - Proveas y recibas ayuda usando el ambiente de computación.
  - Participes, junto con otros estudiantes, en discuiones hacer de las tareas, requerimientos, estrategais de solución, etc.


- Eres culpable de romper el código de conducta si ...
  - Le das tu código fuente a cualquiera en formato electrónico o analógico.
  - Recibes de otro estudiante la solución en formato electrónico o analógico.
  - Subes al repositorio como tuyos otros archivos, soluciones o documentos.
  - Subes tareas sin indicar que colaboraste con alguien.
  - Realizas modificaciones al código en un esfuerzo de ocultar un engaño.
  - Usas material no permitido en examen o te comunicas con alguien de manera no autorizada durante el examen.

* Ligas de interes

- [[http://git-scm.com/book][libro de Git]]
- [[https://rstudio-education.github.io/hopr/][Hands-On Programing with R]]
